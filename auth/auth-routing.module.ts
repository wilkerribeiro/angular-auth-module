import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SetTokenComponent } from './components/set-token/set-token.component';
import { RemoveTokenComponent } from './components/remove-token/remove-token.component';

const routes: Routes = [
	{
		path: '', children: [
			{ path: 'set-token', component: SetTokenComponent },
			{ path: 'remove-token', component: RemoveTokenComponent }
		]
	}
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class AuthRoutingModule { }
