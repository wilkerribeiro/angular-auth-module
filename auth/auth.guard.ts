import { AuthService } from './auth.service';
import { Injectable } from '@angular/core';
import { Route, CanLoad, RouterStateSnapshot, ActivatedRouteSnapshot, Router, CanActivate } from '@angular/router';

@Injectable()
export class AuthGuard implements CanActivate {
	constructor(private router: Router, private authService: AuthService) { }

	canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
		if (localStorage.getItem(this.authService.localStorageId)) {
			return true;
		}

		const redirectUrl = state.url;
		// Manda pra página de login
		window.location.href = this.authService.ssoUrl +'/auth/check' + '?redirect-url=' + redirectUrl + '&sistema-key=' + this.authService.sistemaKey;
		return false;
	}

}
