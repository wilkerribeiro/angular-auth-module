import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../auth.service';
import {Params, Router,  ActivatedRoute} from '@angular/router';

@Component({
	selector: 'app-sso-auth-remove-token',
	templateUrl: './remove-token.component.html',
	styleUrls: ['./remove-token.component.css']
})
export class RemoveTokenComponent implements OnInit {

	constructor(
		private authService: AuthService,
		private route: ActivatedRoute,
		private router: Router
	) { }
	ngOnInit(): void {

		this.route.queryParams.subscribe((params: Params) => {
			const state = params["redirect-url"];
			this.authService.removetoken();
			this.router.navigate([state])
		});
	}
}
