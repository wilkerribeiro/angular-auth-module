import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../auth.service';
import {Params, Router,  ActivatedRoute} from '@angular/router';

@Component({
	selector: 'app-sso-auth-set-token',
	templateUrl: './set-token.component.html',
	styleUrls: ['./set-token.component.css']
})
export class SetTokenComponent implements OnInit {

	constructor(
		private authService: AuthService,
		private route: ActivatedRoute,
		private router: Router
	) { }
	ngOnInit(): void {

		this.route.queryParams.subscribe((params: Params) => {
			const token = params["token"]
			const state = params["redirect-url"];
			this.authService.setToken(token, null)
			this.router.navigate([state])
		});
	}
}
