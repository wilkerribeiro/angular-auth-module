import { SetTokenComponent } from './components/set-token/set-token.component';
import { RemoveTokenComponent } from './components/remove-token/remove-token.component';
import { HttpModule } from '@angular/http';
import { NgModule } from '@angular/core';
import { AuthGuard } from './auth.guard';
import { AuthService } from './auth.service';
import { AuthRoutingModule } from './auth-routing.module';

@NgModule({
	providers: [
		AuthGuard,
	],
	imports: [
		HttpModule,
		AuthRoutingModule
	],
	declarations: [SetTokenComponent, RemoveTokenComponent]
})
export class AuthModule { }
