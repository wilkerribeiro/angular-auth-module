import { Injectable } from '@angular/core';
import { RequestOptions, Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

@Injectable()
export class AuthService {
	public ssoUrl: string
	public token: string;
	public sistemaKey: string;
	public localStorageId: string = 'jwt';
	public redirectLogoutUrl: string = '/'
	constructor(private http: Http) {
		// Pega o token do localStorage e joga na memória
		const currentUser = JSON.parse(localStorage.getItem(this.localStorageId));
		this.token = currentUser && currentUser.token;
	}

	setToken(token, user=null){
		this.token = token;
		localStorage.setItem(this.localStorageId, JSON.stringify({ user: user, token: token }));
	}
	removetoken(): void {
		this.token = null;
		localStorage.removeItem(this.localStorageId);
	}

	logout(): void {
		window.location.href = this.ssoUrl + '/auth/logout' +'?redirect-url=' + this.redirectLogoutUrl + '&sistema-key=' + this.sistemaKey;
	}
}
